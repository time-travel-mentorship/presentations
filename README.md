# List of presentations

* Immutable Objects
* Getting Started with Spring
* JPA Pitfalls
* Theory of broken windows
* How to write meaningful tests

# How
Presentations should last about 30 minutes or less.

# Notes
Frameworks as [reveal.js](https://revealjs.com/) may help to keep the presentations versioned.